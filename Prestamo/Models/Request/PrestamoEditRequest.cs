﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prestamo.Models.Request
{
    public class PrestamoEditRequest
    {
        public int PrestamoId { get; set; }
        public int PersonaId { get; set; }
        public double MontoPrestamo { get; set; }
        public DateTime FechaPago { get; set; }
        public DateTime FechaAutorizacion { get; set; }
        public DateTime FechaEntrega { get; set; }
    }
}
