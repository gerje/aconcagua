﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prestamo.Models.Request
{
    public class PersonaRequest
    {
        public int PersonaId { get; set; }
        public string Documento { get; set; }
        public string Nombre { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
    }
}
