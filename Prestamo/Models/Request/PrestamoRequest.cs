﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prestamo.Models.Request
{
    public class PrestamoRequest
    {
        public int PersonaId { get; set; }
        public double MontoPrestamo { get; set; }
        public DateTime FechaPago { get; set; }
        public DateTime FechaAutorizacion { get; set; }
        public DateTime FechaEntrega { get; set; }

    }
}
