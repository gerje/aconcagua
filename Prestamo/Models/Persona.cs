﻿using System;
using System.Collections.Generic;

namespace Prestamo.Models
{
    public partial class Persona
    {
        public int PersonaId { get; set; }
        public string Documento { get; set; }
        public string Nombre { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
    }
}
