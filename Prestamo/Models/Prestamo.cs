﻿using System;
using System.Collections.Generic;

namespace Prestamo.Models
{
    public partial class Prestamo
    {
        public int PrestamoId { get; set; }
        public string NombrePrestamo { get; set; }
        public int PersonaId { get; set; }
        public double MontoPrestamo { get; set; }
        public DateTime FechaPago { get; set; }
        public DateTime FechaAutorizacion { get; set; }
        public DateTime FechaEntrega { get; set; }
    }
}
