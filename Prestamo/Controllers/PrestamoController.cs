﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Prestamo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("permitir")]
    public class PrestamoController : ControllerBase
    {
        [HttpGet]
        public ActionResult Get()
        {
            using (Models.AconcaguaSFContext db = new Models.AconcaguaSFContext())
            {
                var lst = (from d in db.Prestamo
                           select d).ToList();

                return Ok(lst);
            }
        }

        [HttpGet("{prestamoId}")]
        public ActionResult Get(int prestamoId)
        {
            using (Models.AconcaguaSFContext db = new Models.AconcaguaSFContext())
            {
                var lst = (from d in db.Prestamo
                            where d.PrestamoId == prestamoId
                            select d).FirstOrDefault();

                return Ok(lst);
            }
        }

        [HttpPost]
        public ActionResult Post([FromBody] Models.Request.PrestamoRequest model)
        {
            using (Models.AconcaguaSFContext db = new Models.AconcaguaSFContext())
            {
                Models.Prestamo pres = new Models.Prestamo();
                pres.PersonaId = model.PersonaId;
                pres.MontoPrestamo = model.MontoPrestamo;
                pres.FechaPago = model.FechaPago;
                pres.FechaEntrega = model.FechaEntrega;
                pres.FechaAutorizacion = model.FechaAutorizacion;

                db.Prestamo.Add(pres);
                db.SaveChanges();
            }

            return Ok();
        }

        [HttpPut]
        public ActionResult Put([FromBody] Models.Request.PrestamoEditRequest model)
        {
            using (Models.AconcaguaSFContext db = new Models.AconcaguaSFContext())
            {
                Models.Prestamo pres = db.Prestamo.Find(model.PrestamoId);
                pres.PersonaId = model.PersonaId;
                pres.MontoPrestamo = model.MontoPrestamo;
                pres.FechaPago = model.FechaPago;
                pres.FechaEntrega = model.FechaEntrega;
                pres.FechaAutorizacion = model.FechaAutorizacion;

                db.Entry(pres).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
            }

            return Ok();
        }

        [HttpDelete]
        public ActionResult Delete([FromBody] Models.Request.PrestamoEditRequest model)
        {
            using (Models.AconcaguaSFContext db = new Models.AconcaguaSFContext())
            {
                Models.Prestamo pres = db.Prestamo.Find(model.PrestamoId);
                db.Prestamo.Remove(pres);
                db.SaveChanges();
            }

            return Ok();
        }
    }
}