﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Prestamo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("permitir")]
    public class PersonaController : ControllerBase
    {
        [HttpGet]
        public ActionResult Get()
        {
            using (Models.AconcaguaSFContext db = new Models.AconcaguaSFContext())
            {
                var lst = (from d in db.Persona
                           select d).ToList();

                return Ok(lst);
            }
        }

        [HttpGet("{personaId}")]
        public ActionResult Get(int personaId)
        {
            using (Models.AconcaguaSFContext db = new Models.AconcaguaSFContext())
            {
                var lst = (from d in db.Persona
                           where d.PersonaId == personaId 
                           select d).FirstOrDefault();

                return Ok(lst);
            }
        }

        [HttpPost]
        public ActionResult Post([FromBody] Models.Request.PersonaRequest model)
        {
            using (Models.AconcaguaSFContext db = new Models.AconcaguaSFContext())
            {
                Models.Persona pres = new Models.Persona();
                pres.Documento = model.Documento;
                pres.Nombre = model.Nombre;
                pres.PrimerApellido = model.PrimerApellido;
                pres.SegundoApellido = model.SegundoApellido;
                pres.Telefono = model.Telefono;
                pres.Celular = model.Celular;

                db.Persona.Add(pres);
                db.SaveChanges();
            }

            return Ok();
        }

        [HttpPut]
        public ActionResult Put([FromBody] Models.Request.PersonaRequest model)
        {
            using (Models.AconcaguaSFContext db = new Models.AconcaguaSFContext())
            {
                Models.Persona pres = db.Persona.Find(model.PersonaId);
                pres.Documento = model.Documento;
                pres.Nombre = model.Nombre;
                pres.PrimerApellido = model.PrimerApellido;
                pres.SegundoApellido = model.SegundoApellido;
                pres.Telefono = model.Telefono;
                pres.Celular = model.Celular;

                db.Entry(pres).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
            }

            return Ok();
        }

        [HttpDelete]
        public ActionResult Delete([FromBody] Models.Request.PersonaRequest model)
        {
            using (Models.AconcaguaSFContext db = new Models.AconcaguaSFContext())
            {
                Models.Persona pres = db.Persona.Find(model.PersonaId);
                db.Persona.Remove(pres);
                db.SaveChanges();
            }

            return Ok();
        }
    }
}